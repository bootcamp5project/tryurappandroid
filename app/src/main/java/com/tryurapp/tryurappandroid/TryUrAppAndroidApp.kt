package com.tryurapp.tryurappandroid

import android.support.multidex.MultiDexApplication
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.tryurapp.tryurappandroid.router.Router


class TryUrAppAndroidApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        //Begins app
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        //Router().navigateFromTryUrAppAndroidAppToSettingsAppClientActivity(this)
        Router().navigateFromTryUrAppAndroidAppToTabBarTUAActivity(this)
        //Router().navigateToLogin(this)
    }

    override fun onLowMemory() {
        super.onLowMemory()
    }
}