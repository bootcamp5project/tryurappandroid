package com.tryurapp.tryurappandroid.router

import android.content.Intent
import com.tryurapp.tryurappandroid.INTENT_POST_DETAIL
import com.tryurapp.tryurappandroid.INTENT_PRODUCT_DETAIL
import com.tryurapp.tryurappandroid.TryUrAppAndroidApp
import com.tryurapp.tryurappandroid.apptua.activity.TabBarTUAActivity
import com.tryurapp.tryurappandroid.endclientapp.activity.PostDetailActivity
import com.tryurapp.tryurappandroid.endclientapp.activity.ProductDetailActivity
import com.tryurapp.tryurappandroid.endclientapp.activity.TabBarActivity
import com.tryurapp.tryurappandroid.apptua.login.LoginActivity
import com.tryurapp.tryurappandroid.model.PostUI
import com.tryurapp.tryurappandroid.model.ProductUI

class Router {

    fun navigateFromTryUrAppAndroidAppToTabBarTUAActivity(currentActivity: TryUrAppAndroidApp) {
        val intent = Intent(currentActivity, TabBarTUAActivity::class.java)
        currentActivity.startActivity(intent)
    }
/*
    fun navigateFromLoginToSettingsAppClientActivity(currentActivity: LoginActivity) {
        val intent = Intent(currentActivity, SettingsAppClientActivity::class.java)
        currentActivity.startActivity(intent)
    }
*/
    fun navigateFromLoginToTabBarTUAActivity(currentActivity: LoginActivity) {
        val intent = Intent(currentActivity, TabBarTUAActivity::class.java)
        currentActivity.startActivity(intent)
    }

    fun navigateFromProductListToProductDetail(currentActivity: TabBarActivity,
                                               product: ProductUI) {
        val intent = Intent(currentActivity, ProductDetailActivity::class.java)
        intent.putExtra(INTENT_PRODUCT_DETAIL, product)
        currentActivity.startActivity(intent)
    }

    fun navigateFromTabBarTUAActivityToTabBarActivity(currentActivity: TabBarTUAActivity) {
        val intent = Intent(currentActivity, TabBarActivity::class.java)
        currentActivity.startActivity(intent)
    }

    fun navigateFromPostListToPostDetail(currentActivity: TabBarActivity,
                                         post: PostUI) {
        val intent = Intent(currentActivity, PostDetailActivity::class.java)
        intent.putExtra(INTENT_POST_DETAIL, post)
        currentActivity.startActivity(intent)
    }


}