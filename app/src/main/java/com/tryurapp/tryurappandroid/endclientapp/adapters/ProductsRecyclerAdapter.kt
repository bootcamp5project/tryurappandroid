package com.tryurapp.tryurappandroid.endclientapp.adapters

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.tryurapp.tryurappandroid.R
import com.tryurapp.tryurappandroid.model.ProductUI
import com.tryurapp.tryurappandroid.model.ProductsUI

class ProductsRecyclerAdapter( val products: ProductsUI): RecyclerView.Adapter<ProductsRecyclerAdapter.ProductViewHolder>() {

    var onClickListener: View.OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ProductViewHolder{
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.product_cell, parent, false)

        view.setOnClickListener(onClickListener)

        return ProductViewHolder(view)
    }

    override fun getItemCount(): Int {
        return products.count()
    }

    override fun onBindViewHolder(holder: ProductViewHolder?, position: Int) {
        holder?.bindProduct(products.get(position))
    }

    inner class ProductViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val productImage = itemView.findViewById<ImageView>(R.id.product_image)
        val productName = itemView.findViewById<TextView>(R.id.product_name)
        val productPrice = itemView.findViewById<TextView>(R.id.product_price)
        val currencySymbol = '€'

        fun bindProduct(product: ProductUI) {
            productPrice.setTypeface(null, Typeface.BOLD)
            productName.text = product.name
            productPrice.text = product.price.toString() + ' ' + currencySymbol
            Picasso.with(itemView.context)
                    .load(product.image)
                    .placeholder(R.drawable.placeholder)
                    .into(productImage)
        }
    }
}