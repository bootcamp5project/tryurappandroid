package com.tryurapp.tryurappandroid.endclientapp.fragments


import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.R
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.endclientapp.adapters.ProductsRecyclerAdapter
import com.tryurapp.tryurappandroid.interactors.getallProducts.GetAllProductsInteractorImpl
import com.tryurapp.tryurappandroid.model.ProductUI
import com.tryurapp.tryurappandroid.model.ProductsUI
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_product_list.view.*


class ProductListFragment: Fragment() {

    lateinit var root: View
    private lateinit var productsRecycler: RecyclerView
    var onProductCellSelectedListener: ProductListFragment.OnProductCellSelectedListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        root = inflater!!.inflate(R.layout.fragment_product_list, container, false)

        //Descargo los productos
        downloadProducts()

        return root
    }

    private fun downloadProducts() {
        root.product_list_progress_bar.visibility = View.VISIBLE
        val getProducts = GetAllProductsInteractorImpl(activity)

        getProducts.loadProducts(success = object: SuccessCompletion<Flowable<ProductsUI>> {
            override fun successCompletion(e: Flowable<ProductsUI>) {
                e.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy {
                            initializeProductList(it)
                            root.product_list_progress_bar.visibility = View.INVISIBLE
                        }
            }

        }, error = object: ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                Toast.makeText(activity,
                            errorMessage,
                            Toast.LENGTH_LONG).show();
                        root.product_list_progress_bar.visibility = View.INVISIBLE
            }
        })
    }

    private fun initializeProductList(products: ProductsUI) {
        if (products.count() == 0) {
            Toast.makeText(activity, "No hay productos", Toast.LENGTH_LONG).show()
        }
        productsRecycler = root.findViewById(R.id.products_recycler_view) as RecyclerView
        productsRecycler.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        productsRecycler.itemAnimator = DefaultItemAnimator()
        productsRecycler.layoutManager = GridLayoutManager(activity, 2)
        val adapter = ProductsRecyclerAdapter(products)
        adapter.onClickListener = View.OnClickListener {
            val position = productsRecycler.getChildAdapterPosition(it)
            val product = products.get(position)
            onProductCellSelectedListener?.productCellSelected(product)
        }
        productsRecycler.adapter = adapter
    }

    //Navegacion al detalle
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        commonOnAttach(context)
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        commonOnAttach(activity)
    }

    fun commonOnAttach(context: Context?) {
        if (context is OnProductCellSelectedListener) {
            onProductCellSelectedListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        onProductCellSelectedListener = null
    }

    interface OnProductCellSelectedListener {
        fun productCellSelected(product: ProductUI)
    }
}
