package com.tryurapp.tryurappandroid.endclientapp.fragments


import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.R
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.endclientapp.adapters.PostsRecyclerAdapter
import com.tryurapp.tryurappandroid.interactors.getallposts.GetAllPostsInteractorImpl
import com.tryurapp.tryurappandroid.model.PostUI
import com.tryurapp.tryurappandroid.model.PostsUI
import kotlinx.android.synthetic.main.fragment_post_list.view.*

class PostListFragment: Fragment() {

    lateinit var root: View
    private lateinit var postsRecycler: RecyclerView
    var onPostCellSelectedListener: OnPostCellSelectedListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater!!.inflate(R.layout.fragment_post_list, container, false)

        //Descargo los posts
        downloadPosts()

        return root
    }

    private fun downloadPosts() {
        root.post_list_progress_bar.visibility = View.VISIBLE
        val getPosts = GetAllPostsInteractorImpl(activity)
        getPosts.loadPosts(
                success = object: SuccessCompletion<PostsUI> {
                    override fun successCompletion(e: PostsUI) {
                        initialicePostList(e)
                        root.post_list_progress_bar.visibility = View.INVISIBLE
                    }
                }, error = object: ErrorCompletion {
                    override fun errorCompletion(errorMessage: String) {
                        println(errorMessage)
                        Toast.makeText(activity,
                            errorMessage,
                            Toast.LENGTH_LONG).show()
                        root.post_list_progress_bar.visibility = View.INVISIBLE
                    }
                }
        )
    }

    private fun initialicePostList(posts: PostsUI) {
        postsRecycler = root.findViewById(R.id.posts_recycler_view)
        postsRecycler.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        postsRecycler.itemAnimator = DefaultItemAnimator()
        postsRecycler.layoutManager = GridLayoutManager(activity, 1)
        val adapter = PostsRecyclerAdapter(posts)
        adapter.onClickListener = View.OnClickListener {
            val position = postsRecycler.getChildAdapterPosition(it)
            val post = posts.get(position)
            onPostCellSelectedListener?.postCellSelected(post)
        }
        postsRecycler.adapter = adapter
    }

    //Navegacion al detalle
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        commonOnAttach(context)
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        commonOnAttach(activity)
    }

    fun commonOnAttach(context: Context?) {
        if (context is OnPostCellSelectedListener) {
            onPostCellSelectedListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        onPostCellSelectedListener = null
    }

    interface OnPostCellSelectedListener {
        fun postCellSelected(post: PostUI)
    }
}
