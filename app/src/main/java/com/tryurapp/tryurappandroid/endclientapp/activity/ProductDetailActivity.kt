package com.tryurapp.tryurappandroid.endclientapp.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.tryurapp.tryurappandroid.INTENT_PRODUCT_DETAIL
import com.tryurapp.tryurappandroid.R
import com.tryurapp.tryurappandroid.model.ProductUI
import kotlinx.android.synthetic.main.activity_product_detail.*

class ProductDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        //Activo el botón de back de la barra
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "AppClient"

        //Recoger el producto
        val product = intent.getSerializableExtra(INTENT_PRODUCT_DETAIL) as ProductUI

        //Mostrar el producto en vista
        product_detail_name.text = product.name

    }

    //Si pulsan el botón Back, salgo de la actividad sin mas y vuelvo a la anterior
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
