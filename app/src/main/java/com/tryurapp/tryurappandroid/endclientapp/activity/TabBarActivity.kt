package com.tryurapp.tryurappandroid.endclientapp.activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import com.tryurapp.tryurappandroid.R
import com.tryurapp.tryurappandroid.endclientapp.fragments.PostListFragment
import com.tryurapp.tryurappandroid.endclientapp.fragments.ProductListFragment
import com.tryurapp.tryurappandroid.endclientapp.fragments.SettingsFragment
import com.tryurapp.tryurappandroid.endclientapp.fragments.UsersFragment
import com.tryurapp.tryurappandroid.model.PostUI
import com.tryurapp.tryurappandroid.model.ProductUI
import com.tryurapp.tryurappandroid.router.Router

class TabBarActivity : AppCompatActivity(), PostListFragment.OnPostCellSelectedListener,
                       ProductListFragment.OnProductCellSelectedListener{

    private lateinit var postsRecycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_bar)

        //Activo el botón de back de la barra
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "AppClient"

        //Meto el tabBar
        val bottomNavigationView = findViewById<View>(R.id.bottom_navigation) as BottomNavigationView
        bottomNavigationView.setSelectedItemId(R.id.menu_blog)
        putBlogInContainer()
        bottomNavigationView.setOnNavigationItemSelectedListener(
                object : BottomNavigationView.OnNavigationItemSelectedListener {
                    override fun onNavigationItemSelected(item: MenuItem): Boolean {
                        when (item.getItemId()) {
                            R.id.menu_blog -> putBlogInContainer()
                            R.id.menu_ecommerce -> putECommerceInContainer()
                            R.id.menu_users -> putUsersInContainer()
                            R.id.menu_settings -> putSettingsInContainer()
                        }
                        return true
                    }
                })
    }

    //Si pulsan el botón Back, salgo de la actividad sin mas y vuelvo a la anterior
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun putBlogInContainer() {
        // Metemos el PostListFragment en nuestra interfaz si es necesario
        if (fragmentManager.findFragmentById(R.id.container_fragment) == null) {
            // Si hemos llegado aquí, sabemos que nunca hemos creado el Fragment, lo creamos
            fragmentManager.beginTransaction()
                    .add(R.id.container_fragment, PostListFragment())
                    .commit()
        } else {
            // Si hemos llegado aquí, sabemos que hemos creado el Fragment. Colocamos el que queremos
            fragmentManager.beginTransaction()
                    .replace(R.id.container_fragment, PostListFragment())
                    .commit()
        }
    }

    fun putECommerceInContainer() {
        // Metemos el ProductListFragment en nuestra interfaz si es necesario
        if (fragmentManager.findFragmentById(R.id.container_fragment) == null) {
            // Si hemos llegado aquí, sabemos que nunca hemos creado el ProductListFragment, lo creamos
            fragmentManager.beginTransaction()
                    .add(R.id.container_fragment, ProductListFragment())
                    .commit()
        } else {
            // Si hemos llegado aquí, sabemos que hemos creado el Fragment. Colocamos el que queremos
            fragmentManager.beginTransaction()
                    .replace(R.id.container_fragment, ProductListFragment())
                    .commit()
        }
    }

    fun putUsersInContainer() {
        // Metemos el UsersFragment en nuestra interfaz si es necesario
        if (fragmentManager.findFragmentById(R.id.container_fragment) == null) {
            // Si hemos llegado aquí, sabemos que nunca hemos creado el UsersFragment, lo creamos
            fragmentManager.beginTransaction()
                    .add(R.id.container_fragment, UsersFragment())
                    .commit()
        } else {
            // Si hemos llegado aquí, sabemos que hemos creado el Fragment. Colocamos el que queremos
            fragmentManager.beginTransaction()
                    .replace(R.id.container_fragment, UsersFragment())
                    .commit()
        }
    }

    fun putSettingsInContainer() {
        // Metemos el SettingsFragment en nuestra interfaz si es necesario
        if (fragmentManager.findFragmentById(R.id.container_fragment) == null) {
            // Si hemos llegado aquí, sabemos que nunca hemos creado el SettingsFragment, lo creamos
            fragmentManager.beginTransaction()
                    .add(R.id.container_fragment, SettingsFragment())
                    .commit()
        } else {
            // Si hemos llegado aquí, sabemos que hemos creado el Fragment. Colocamos el que queremos
            fragmentManager.beginTransaction()
                    .replace(R.id.container_fragment, SettingsFragment())
                    .commit()
        }
    }

    //Cuando el PostListFragment avisa de que se ha pulsado una celda, vamos al detalle
    override fun postCellSelected(post: PostUI) {
        Router().navigateFromPostListToPostDetail(this, post)
    }

    //Cuando el ProductListFragment avisa de que se ha pulsado una celda, vamos al detalle
    override fun productCellSelected(product: ProductUI) {
        Router().navigateFromProductListToProductDetail(this, product)
    }


}