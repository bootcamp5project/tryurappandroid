package com.tryurapp.tryurappandroid.endclientapp.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.tryurapp.tryurappandroid.INTENT_POST_DETAIL
import com.tryurapp.tryurappandroid.R
import com.tryurapp.tryurappandroid.model.PostUI
import kotlinx.android.synthetic.main.activity_post_detail.*

class PostDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_detail)

        //Activo el botón de back de la barra
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "AppClient"

        //Recoger el producto
        val post = intent.getSerializableExtra(INTENT_POST_DETAIL) as PostUI

        //Mostrar el producto en vista
        post_detail_name.text = post.title
    }

    //Si pulsan el botón Back, salgo de la actividad sin mas y vuelvo a la anterior
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
