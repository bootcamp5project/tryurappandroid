package com.tryurapp.tryurappandroid.endclientapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.tryurapp.tryurappandroid.R
import com.tryurapp.tryurappandroid.model.PostUI
import com.tryurapp.tryurappandroid.model.PostsUI

class PostsRecyclerAdapter( val posts: PostsUI): RecyclerView.Adapter<PostsRecyclerAdapter.PostViewHolder>() {

    var onClickListener: View.OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PostViewHolder{
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.blog_cell, parent, false)

        view.setOnClickListener(onClickListener)

        return PostViewHolder(view)
    }

    override fun getItemCount(): Int {
        return posts.count()
    }

    override fun onBindViewHolder(holder: PostViewHolder?, position: Int) {
        holder?.bindPost(posts.get(position))
    }

    inner class PostViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val blogPublishedOn = itemView.findViewById<TextView>(R.id.blog_published_on)
        val blogTitle = itemView.findViewById<TextView>(R.id.blog_title)
        val blogPhoto = itemView.findViewById<ImageView>(R.id.blog_photo)
        val blogWriter = itemView.findViewById<TextView>(R.id.blog_writer)
        val blogPublishedIn = itemView.findViewById<TextView>(R.id.blog_published_in)
        val blogComments = itemView.findViewById<TextView>(R.id.blog_comments)
        val blogInputs = itemView.findViewById<TextView>(R.id.blog_input)

        fun bindPost(blog: PostUI) {
            blogPublishedOn.text = blog.publishedOn
            blogTitle.text = blog.title
            blogWriter.text = blog.writer
            blogPublishedIn.text = blog.publishedIn
            blogComments.text = blog.comments
            blogInputs.text = blog.input
            Picasso.with(itemView.context)
                    .load(blog.photo)
                    .placeholder(R.drawable.placeholder)
                    .into(blogPhoto)
        }
    }
}