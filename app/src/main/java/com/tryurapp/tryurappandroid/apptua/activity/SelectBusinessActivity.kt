package com.tryurapp.tryurappandroid.apptua.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.tryurapp.tryurappandroid.R

class SelectBusinessActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_business)

        //Activo el botón de back de la barra
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "AppClient"

        //Recoger el producto
        //val post = intent.getSerializableExtra(INTENT_SELECT_BUSINESS) as PostUI

        //Mostrar el producto en vista
        //post_detail_name.text = post.title
    }

    //Si pulsan el botón Back, salgo de la actividad sin mas y vuelvo a la anterior
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}