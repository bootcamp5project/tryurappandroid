package com.tryurapp.tryurappandroid.apptua.fragments

import android.app.AlertDialog
import android.app.Fragment
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.R
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.interactors.addpost.AddPostInteractorImpl
import com.tryurapp.tryurappandroid.model.PostUI
import com.tryurapp.tryurappandroid.repository.model.ResponseApi
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseApiTUA
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessTUA
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessesTUA
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseUsersApiTUA
import com.tryurapp.tryurappandroid.tuainteractors.adduser.AddUserInteractorImpl
import com.tryurapp.tryurappandroid.tuainteractors.authenticateuser.AuthenticateUserInteractorImpl
import com.tryurapp.tryurappandroid.tuainteractors.createbusiness.CreateBusinessInteractorImpl
import com.tryurapp.tryurappandroid.tuainteractors.deleteuser.DeleteUserInteractorImpl
import com.tryurapp.tryurappandroid.tuainteractors.getallbusiness.GetAllBusinessInteractorImpl
import com.tryurapp.tryurappandroid.tuainteractors.getallusers.GetAllUsersInteractorImpl
import com.tryurapp.tryurappandroid.tuainteractors.getbusiness.GetBusinessInteractorImpl
import com.tryurapp.tryurappandroid.tuainteractors.getuser.GetUserInteractorImpl
import com.tryurapp.tryurappandroid.tuainteractors.savebusinesssettings.SaveBusinessSettingsInteractorImpl
import com.tryurapp.tryurappandroid.tuainteractors.updateuser.UpdateUserInteractorImpl
import com.tryurapp.tryurappandroid.tuainteractors.uploadImage.UploadImageInteractorImpl
import com.tryurapp.tryurappandroid.tuamodel.BusinessUI
import com.tryurapp.tryurappandroid.tuamodel.UserUI
import kotlinx.android.synthetic.main.fragment_testapi.*
import kotlinx.android.synthetic.main.fragment_testapi.view.*

class TestAPIFragment: Fragment() {

    lateinit var root: View

    private val user = UserUI(
            "q1w1@gmail.com",
            "Fernando",
            "Jarilla1",
            ""
    )

    private val business = BusinessUI(
            "q1w1@gmail.com",
            "https://www.business1.com",
            1,
            1,
            1,
            1,
            1,
            1,
            11,
            1,
            true,
            "logo",
            "welcome",
            "android",
            "TryUrApp"
    )

    private val post = PostUI(
            1,
            "Mi primer POST para añadir un post",
            "",
            "Texto del post",
            "",
            "",
            "",
            ""
    )

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater!!.inflate(R.layout.fragment_testapi, container, false)

        testOptions()

        return root
    }

    fun testOptions() {
        root.button_authenticate.setOnClickListener {
            authUser()
        }

        root.button_get_all_users.setOnClickListener{
            getUsers()
        }

        root.button_add.setOnClickListener{
            addUser()
        }

        root.button_get_user.setOnClickListener {
            getUser()
        }

        root.button_update.setOnClickListener {
            updateUser()
        }

        root.button_delete.setOnClickListener {
            deleteUser()
        }

        root.button_get_business.setOnClickListener {
            getBusiness()
        }

        root.button_get_all_business.setOnClickListener {
            getAllBusiness()
        }
        root.button_create_business.setOnClickListener {
            createBusiness()
        }

        root.button_save_business_settings.setOnClickListener {
            saveBusinessSettings()
        }

        root.button_build.setOnClickListener {
            var nameText = name_text.text.toString()
            if (nameText == "") { nameText = "TryUrApp" }
            buildApplication(nameText)
        }

        root.button_upload_logo.setOnClickListener {
            uploadLogo()
        }

        root.button_upload_welcome.setOnClickListener {
            uploadWelcome()
        }
    }

    fun alert(message: String) {
        AlertDialog.Builder(activity)
                .setTitle("INFORMACION")
                .setMessage(message)
                .setPositiveButton("OK", { dialog, witch ->
                    dialog.dismiss()
                })
                .show()
    }

    fun alertError(message: String) {
        AlertDialog.Builder(activity)
                .setTitle("ERROR")
                .setMessage(message)
                .setPositiveButton("OK", { dialog, witch ->
                    dialog.dismiss()
                })
                .show()
    }

    fun addPost() {
        val addPost = AddPostInteractorImpl(activity, post)
        addPost.addPost(
                success = object: SuccessCompletion<ResponseApi> {
                    override fun successCompletion(e: ResponseApi) {
                        alert("ADD POST: " + e.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun authUser() {
        val authUser = AuthenticateUserInteractorImpl(activity, user)
        authUser.authUser(
                success = object: SuccessCompletion<ResponseApiTUA> {
                    override fun successCompletion(e: ResponseApiTUA) {
                        alert("TOKEN: " + e.resultDescription)
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun getUsers() {
        val getAllUsers = GetAllUsersInteractorImpl(activity)
        getAllUsers.getAllUsers(
                success = object: SuccessCompletion<ResponseUsersApiTUA> {
                    override fun successCompletion(e: ResponseUsersApiTUA) {
                        alert("USERS: " + e.resultDescription)
                        Log.d("Users: ", e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun addUser() {
        val addUser = AddUserInteractorImpl(activity, user)
        addUser.addUser(
                success = object: SuccessCompletion<ResponseApiTUA> {
                    override fun successCompletion(e: ResponseApiTUA) {
                        alert("USER ADDED: " + e.resultDescription)
                        Log.d("User ADDED: ", e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun getUser() {
        val getUser = GetUserInteractorImpl(activity, user)
        getUser.getUser(
                success = object: SuccessCompletion<ResponseApiTUA> {
                    override fun successCompletion(e: ResponseApiTUA) {
                        alert("USER GETTED: " + e.resultDescription)
                        Log.d("User GETTED: ",e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun updateUser() {
        user.name = "name updated"
        val updateUser = UpdateUserInteractorImpl(activity, user)
        updateUser.updateUser(
                success = object: SuccessCompletion<ResponseApiTUA> {
                    override fun successCompletion(e: ResponseApiTUA) {
                        alert("USER UPDATED: " + e.resultDescription)
                        Log.d("User UPDATED): ",e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun deleteUser() {
        val deleteUser = DeleteUserInteractorImpl(activity, user)
        deleteUser.deleteUser(
                success = object: SuccessCompletion<ResponseApiTUA> {
                    override fun successCompletion(e: ResponseApiTUA) {
                        alert("USER DELETED: " + e.resultDescription)
                        Log.d("User DELETED: ", e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun createBusiness() {
        val createBusiness = CreateBusinessInteractorImpl(activity, user, business)
        createBusiness.createBusiness(
                success = object: SuccessCompletion<ResponseBusinessTUA> {
                    override fun successCompletion(e: ResponseBusinessTUA) {
                        alert("Business CREATED: " + e.resultDescription)
                        Log.d("Business CREATED: ", e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun saveBusinessSettings() {
        val saveBusinessSettings = SaveBusinessSettingsInteractorImpl(activity, user, business)
        saveBusinessSettings.saveBusinessSettings(
                success = object: SuccessCompletion<ResponseBusinessTUA> {
                    override fun successCompletion(e: ResponseBusinessTUA) {
                        alert("Business SAVED: " + e.resultDescription)
                        Log.d("Business SAVED: ", e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun getBusiness() {
        val getBusiness = GetBusinessInteractorImpl(activity, user, business)
        getBusiness.getBusiness(
                success = object: SuccessCompletion<ResponseBusinessTUA> {
                    override fun successCompletion(e: ResponseBusinessTUA) {
                        alert("Business: " + e.resultDescription)
                        Log.d("Business: ", e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun getAllBusiness() {
        val getAllBusiness = GetAllBusinessInteractorImpl(activity)
        getAllBusiness.getAllBusiness(
                success = object: SuccessCompletion<ResponseBusinessesTUA> {
                    override fun successCompletion(e: ResponseBusinessesTUA) {
                        alert("All Business: " + e.resultDescription)
                        Log.d("All Business: ", e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun uploadLogo() {
        val uploadImage = UploadImageInteractorImpl(activity, business, user,
                "logotua.png", "logo")
        uploadImage.uploadImage(
                success = object: SuccessCompletion<ResponseBusinessTUA> {
                    override fun successCompletion(e: ResponseBusinessTUA) {
                        alert("Upload: " + e.resultDescription)
                        Log.d("Upload: ", e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun uploadWelcome() {
        val uploadImage = UploadImageInteractorImpl(activity, business, user,
                "welcomepage.png", "welcomeImage")
        uploadImage.uploadImage(
                success = object: SuccessCompletion<ResponseBusinessTUA> {
                    override fun successCompletion(e: ResponseBusinessTUA) {
                        alert("Upload: " + e.resultDescription)
                        Log.d("Upload: ", e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }

    fun buildApplication(name: String) {
        business.businessName = name
        business.buildApp = "android"
        val saveBusinessSettings = SaveBusinessSettingsInteractorImpl(activity, user, business)
        saveBusinessSettings.saveBusinessSettings(
                success = object: SuccessCompletion<ResponseBusinessTUA> {
                    override fun successCompletion(e: ResponseBusinessTUA) {
                        alert("Business SAVED: " + e.resultDescription)
                        Log.d("Business SAVED: ", e.result.toString())
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                alertError(errorMessage)
            }
        }
        )
    }
}