package com.tryurapp.tryurappandroid.apptua.activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import com.tryurapp.tryurappandroid.R
import com.tryurapp.tryurappandroid.apptua.fragments.BusinessFragment
import com.tryurapp.tryurappandroid.apptua.fragments.PreviewFragment
import com.tryurapp.tryurappandroid.apptua.fragments.ProfileFragment
import com.tryurapp.tryurappandroid.apptua.fragments.TestAPIFragment
import com.tryurapp.tryurappandroid.router.Router

class TabBarTUAActivity : AppCompatActivity(), PreviewFragment.OnFragmentButtonPressedListener {

    private lateinit var postsRecycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_bar_tua)

        /*
        //Activo el botón de back de la barra
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        */


        //Meto el tabBar
        val bottomNavigationView = findViewById<View>(R.id.bottom_navigation) as BottomNavigationView
        bottomNavigationView.setSelectedItemId(R.id.menu_blog)
        putBusinessInContainer()
        bottomNavigationView.setOnNavigationItemSelectedListener(
                object : BottomNavigationView.OnNavigationItemSelectedListener {
                    override fun onNavigationItemSelected(item: MenuItem): Boolean {
                        when (item.getItemId()) {
                            R.id.menu_business -> putBusinessInContainer()
                            R.id.menu_testapi -> putTestAPIInContainer()
                            R.id.menu_profile -> putProfileInContainer()
                            R.id.menu_preview -> putPreviewInContainer()
                        }
                        return true
                    }
                })
    }

    //Si pulsan el botón Back, salgo de la actividad sin mas y vuelvo a la anterior
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun putBusinessInContainer() {

        supportActionBar?.title = "Business"

        // Metemos el BusinessFragment en nuestra interfaz si es necesario
        if (fragmentManager.findFragmentById(R.id.container_fragment) == null) {
            // Si hemos llegado aquí, sabemos que nunca hemos creado el Fragment, lo creamos
            fragmentManager.beginTransaction()
                    .add(R.id.container_fragment, BusinessFragment())
                    .commit()
        } else {
            // Si hemos llegado aquí, sabemos que hemos creado el Fragment. Colocamos el que queremos
            fragmentManager.beginTransaction()
                    .replace(R.id.container_fragment, BusinessFragment())
                    .commit()
        }
    }

    fun putTestAPIInContainer() {

        supportActionBar?.title = "Test API"

        // Metemos el ProductListFragment en nuestra interfaz si es necesario
        if (fragmentManager.findFragmentById(R.id.container_fragment) == null) {
            // Si hemos llegado aquí, sabemos que nunca hemos creado el ProductListFragment, lo creamos
            fragmentManager.beginTransaction()
                    .add(R.id.container_fragment, TestAPIFragment())
                    .commit()
        } else {
            // Si hemos llegado aquí, sabemos que hemos creado el Fragment. Colocamos el que queremos
            fragmentManager.beginTransaction()
                    .replace(R.id.container_fragment, TestAPIFragment())
                    .commit()
        }
    }

    fun putProfileInContainer() {

        supportActionBar?.title = "User Profile"

        // Metemos el UsersFragment en nuestra interfaz si es necesario
        if (fragmentManager.findFragmentById(R.id.container_fragment) == null) {
            // Si hemos llegado aquí, sabemos que nunca hemos creado el UsersFragment, lo creamos
            fragmentManager.beginTransaction()
                    .add(R.id.container_fragment, ProfileFragment())
                    .commit()
        } else {
            // Si hemos llegado aquí, sabemos que hemos creado el Fragment. Colocamos el que queremos
            fragmentManager.beginTransaction()
                    .replace(R.id.container_fragment, ProfileFragment())
                    .commit()
        }
    }

    fun putPreviewInContainer() {

        supportActionBar?.title = "Preview"

        // Metemos el SettingsFragment en nuestra interfaz si es necesario
        if (fragmentManager.findFragmentById(R.id.container_fragment) == null) {
            // Si hemos llegado aquí, sabemos que nunca hemos creado el SettingsFragment, lo creamos
            fragmentManager.beginTransaction()
                    .add(R.id.container_fragment, PreviewFragment())
                    .commit()
        } else {
            // Si hemos llegado aquí, sabemos que hemos creado el Fragment. Colocamos el que queremos
            fragmentManager.beginTransaction()
                    .replace(R.id.container_fragment, PreviewFragment())
                    .commit()
        }
    }

    //Cuando el TestAPIFragment avisa de que se ha pulsado PREVIEW, vamos al detalle
    override fun previewSelected() {
        Router().navigateFromTabBarTUAActivityToTabBarActivity(this)
    }
/*

*/

}