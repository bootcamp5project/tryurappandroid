package com.tryurapp.tryurappandroid.apptua.fragments

import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tryurapp.tryurappandroid.R
import kotlinx.android.synthetic.main.fragment_preview.view.*

class PreviewFragment: Fragment() {

    lateinit var root: View
    var onFragmentButtonPressedListener: PreviewFragment.OnFragmentButtonPressedListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater!!.inflate(R.layout.fragment_preview, container, false)

        root.preview_button.setOnClickListener {
            onFragmentButtonPressedListener?.previewSelected()
        }

        return root
    }

    //Navegacion al PREVIEW
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        commonOnAttach(context)
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        commonOnAttach(activity)
    }

    fun commonOnAttach(context: Context?) {
        if (context is OnFragmentButtonPressedListener) {
            onFragmentButtonPressedListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        onFragmentButtonPressedListener = null
    }

    interface OnFragmentButtonPressedListener {
        fun previewSelected()
    }
}