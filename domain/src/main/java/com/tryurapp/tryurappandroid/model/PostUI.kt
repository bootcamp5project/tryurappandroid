package com.tryurapp.tryurappandroid.model

import java.io.Serializable

/*
*  PostUI -> Represents one post in the User Interface
*/

data class PostUI(
        val id: Int,
        val title: String,
        val publishedOn: String,
        val input: String,
        val writer: String,
        val publishedIn: String = "",
        val comments: String,
        val photo: String
): Serializable

/*
*  PostsUI -> Represents a list of posts in the User Interface
*/

class PostsUI( val postsUI: MutableList<PostUI>): Aggregate<PostUI>, Iterator<PostUI> by postsUI.iterator() {

    override fun count() = postsUI.size

    override fun all() = postsUI

    override fun get(position: Int) = postsUI.get(position)

    override fun get(name: String): PostUI? {
        for (post in 0..postsUI.count() -1) {
            if (postsUI[post].title == name) {
                return postsUI[post]
            }
        }
        return null
    }

    override fun add(element: PostUI) {
        postsUI.add(element)
    }

    override fun delete(position: Int) {
        postsUI.removeAt(position)
    }

    override fun delete(element: PostUI) {
        postsUI.remove(element)
    }
}