package com.tryurapp.tryurappandroid.model

import java.io.Serializable

/*
*  ProductUI -> Represents one product in the User Interface
*/

data class ProductUI(
        val id: Int,
        val name: String,
        val image: String,
        val price: Double
): Serializable

/*
*  ProductsUI -> Represents a list of products in the User Interface
*/

class ProductsUI( val productsUI: MutableList<ProductUI>): Aggregate<ProductUI>, Iterator<ProductUI> by productsUI.iterator() {

    override fun count() = productsUI.size

    override fun all() = productsUI

    override fun get(position: Int) = productsUI.get(position)

    override fun get(name: String): ProductUI? {
        for (product in 0..productsUI.count() -1) {
            if (productsUI[product].name == name) {
                return productsUI[product]
            }
        }
        return null
    }

    override fun add(element: ProductUI) {
        productsUI.add(element)
    }

    override fun delete(position: Int) {
        productsUI.removeAt(position)
    }

    override fun delete(element: ProductUI) {
        productsUI.remove(element)
    }
}