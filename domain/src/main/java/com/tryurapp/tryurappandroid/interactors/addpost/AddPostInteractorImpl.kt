package com.tryurapp.tryurappandroid.interactors.addpost

import android.content.Context
import android.util.Log
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.model.PostUI
import com.tryurapp.tryurappandroid.repository.WPRepository
import com.tryurapp.tryurappandroid.repository.WPRepositoryImpl
import com.tryurapp.tryurappandroid.repository.model.PostBody
import com.tryurapp.tryurappandroid.repository.model.ResponseApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class AddPostInteractorImpl(context: Context, post: PostUI) : AddPostInteractor {


    private val weakContext = WeakReference<Context>(context)
    private val repository: WPRepository = WPRepositoryImpl(weakContext.get()!!)
    private val postToAdd = post

    override fun addPost(success: SuccessCompletion<ResponseApi>,
                         error: ErrorCompletion) {

        repository.addPost(mapperPostUIToPostBody(postToAdd))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            Log.d("Response al POST: ", it.toString())
                            success.successCompletion(it)
                        },
                        onError = {
                            println(it.message)
                            error.errorCompletion(it.message!!)
                        }
                )
    }

    fun mapperPostUIToPostBody(postUI: PostUI): PostBody {

        return PostBody(
                postUI.title
        )
    }
}
