package com.tryurapp.tryurappandroid.interactors.getallProducts

import android.content.Context
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.model.ProductUI
import com.tryurapp.tryurappandroid.model.ProductsUI
import com.tryurapp.tryurappandroid.repository.WPRepository
import com.tryurapp.tryurappandroid.repository.WPRepositoryImpl
import com.tryurapp.tryurappandroid.repository.db.model.ProductObject
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class GetAllProductsInteractorImpl(context: Context): GetAllProductsInteractor {

    private val weakContext = WeakReference<Context>(context)
    private val repository: WPRepository = WPRepositoryImpl(weakContext.get()!!)

    override fun loadProducts(success: SuccessCompletion<Flowable<ProductsUI>>, error:ErrorCompletion) {
        var products: ProductsUI
        repository.getProducts(success = object: SuccessCompletion<Flowable<List<ProductObject>>> {
            override fun successCompletion(e: Flowable<List<ProductObject>>) {

                e.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy {
                            products = mapperProducts(it)
                            success.successCompletion(Flowable.fromArray(products))
                        }
            }

        }, error = object: ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                error.errorCompletion(errorMessage)
            }

        })
    }

    fun mapperProducts(list: List<ProductObject>): ProductsUI {

        val tempList = ArrayList<ProductUI>()
        list.forEach {
            if (it.salePrice == "") { it.salePrice = "0" }
            val product = ProductUI(
                    it.id,
                    it.title,
                    it.image,
                    it.salePrice.toDouble()
            )
            tempList.add(product)
        }
        return ProductsUI(tempList)
    }
}
