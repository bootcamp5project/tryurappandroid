package com.tryurapp.tryurappandroid.interactors.getpost

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repository.model.PostWP

interface GetPostInteractor {
    fun loadPost(success: SuccessCompletion<PostWP>,
                 error: ErrorCompletion)
}