package com.tryurapp.tryurappandroid.interactors.getproduct

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.model.ProductUI
import com.tryurapp.tryurappandroid.repository.model.ProductWP

interface GetProductInteractor {
    fun loadProduct(success: SuccessCompletion<ProductUI>,
                    error: ErrorCompletion)
}
