package com.tryurapp.tryurappandroid.interactors.getallProducts

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.model.ProductsUI
import com.tryurapp.tryurappandroid.repository.db.model.ProductObject
import io.reactivex.Flowable

interface GetAllProductsInteractor {
    fun loadProducts(success: SuccessCompletion<Flowable<ProductsUI>>, error:ErrorCompletion)
}