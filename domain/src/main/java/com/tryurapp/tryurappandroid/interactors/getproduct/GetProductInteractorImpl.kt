package com.tryurapp.tryurappandroid.interactors.getproduct

import android.content.Context
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.model.ProductUI
import com.tryurapp.tryurappandroid.model.ProductsUI
import com.tryurapp.tryurappandroid.repository.WPRepository
import com.tryurapp.tryurappandroid.repository.WPRepositoryImpl
import com.tryurapp.tryurappandroid.repository.model.ProductWP
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class GetProductInteractorImpl(context: Context, productId: Int) : GetProductInteractor {

    private val weakContext = WeakReference<Context>(context)
    private val repository: WPRepository = WPRepositoryImpl(weakContext.get()!!)
    private val productIdToGet = productId

    override fun loadProduct(success: SuccessCompletion<ProductUI>,
                             error: ErrorCompletion) {
        repository.getProduct(productIdToGet)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            val product: ProductUI = mapperProduct(it)
                            success.successCompletion(product)
                        },
                        onError = {
                            error(it)
                        }
                )
    }

    fun mapperProduct(product: ProductWP): ProductUI {

        return ProductUI(
                product.identifier,
                product.name,
                product.images!![0].src,
                product.price.toDouble()
                )
    }
}