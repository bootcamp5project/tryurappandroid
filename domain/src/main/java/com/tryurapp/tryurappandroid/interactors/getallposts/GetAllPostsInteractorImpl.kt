package com.tryurapp.tryurappandroid.interactors.getallposts

import android.content.Context
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.model.PostUI
import com.tryurapp.tryurappandroid.model.PostsUI
import com.tryurapp.tryurappandroid.model.ProductUI
import com.tryurapp.tryurappandroid.model.ProductsUI
import com.tryurapp.tryurappandroid.repository.WPRepository
import com.tryurapp.tryurappandroid.repository.WPRepositoryImpl
import com.tryurapp.tryurappandroid.repository.model.PostWP
import com.tryurapp.tryurappandroid.repository.model.ProductWP
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class GetAllPostsInteractorImpl(context: Context) : GetAllPostsInteractor {

    private val weakContext = WeakReference<Context>(context)
    private val repository: WPRepository = WPRepositoryImpl(weakContext.get()!!)

    override fun loadPosts(success: SuccessCompletion<PostsUI>,
                           error: ErrorCompletion) {
        repository.getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            val posts: PostsUI = mapperPosts(it)
                            success.successCompletion(posts)
                        },
                        onError = {
                            println(it.message)
                            error.errorCompletion(it.message!!)
                        }
                )
    }

    fun mapperPosts(list: List<PostWP>): PostsUI {

        val tempList = ArrayList<PostUI>()
        list.forEach {
            val post = PostUI(
                    it.identifier,
                    it.title!!.rendered,
                    it.date,
                    it.content!!.rendered,
                    it.status,
                    it.date,
                    it.commentStatus,
                    it.link
            )
            tempList.add(post)
        }

        val posts = PostsUI(tempList)
        return posts
    }
}