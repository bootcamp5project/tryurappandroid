package com.tryurapp.tryurappandroid.interactors.getallposts

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.model.PostsUI

interface GetAllPostsInteractor {
    fun loadPosts(success: SuccessCompletion<PostsUI>,
                  error: ErrorCompletion)
}
