package com.tryurapp.tryurappandroid.interactors.addpost

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repository.model.ResponseApi

interface AddPostInteractor {
    fun addPost(success: SuccessCompletion<ResponseApi>,
                  error: ErrorCompletion)
}