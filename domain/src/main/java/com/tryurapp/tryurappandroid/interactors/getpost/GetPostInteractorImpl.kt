package com.tryurapp.tryurappandroid.interactors.getpost

import android.content.Context
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repository.WPRepository
import com.tryurapp.tryurappandroid.repository.WPRepositoryImpl
import com.tryurapp.tryurappandroid.repository.model.PostWP
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class GetPostInteractorImpl(context: Context, postId: Int) : GetPostInteractor {

    private val weakContext = WeakReference<Context>(context)
    private val repository: WPRepository = WPRepositoryImpl(weakContext.get()!!)
    private val postIdToGet = postId

    override fun loadPost(success: SuccessCompletion<PostWP>,
                          error: ErrorCompletion) {
        repository.getPost(postIdToGet)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            println("POST:  " + it)
                        },
                        onError = {
                            println("ERROR:  " + it)
                        }
                )
    }
}