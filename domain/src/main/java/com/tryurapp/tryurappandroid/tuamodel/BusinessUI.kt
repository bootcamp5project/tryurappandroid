package com.tryurapp.tryurappandroid.tuamodel

import com.tryurapp.tryurappandroid.model.Aggregate

data class BusinessUI(
        var userEmail: String,
        var url: String,
        var idBusinessType: Int,
        var idCmsType: Int,
        var idLoadingType: Int,
        var idAppIcon: Int,
        var firstColorIdentifier: Int,
        var secondColorIdentifier: Int,
        var thirdColorIdentifier: Int,
        var idFont: Int,
        var isValidInfo: Boolean,
        var logoDataName: String,
        var welcomeImageDataName: String,
        var buildApp: String,
        var businessName: String
)

class BusinessesUI( val businessesUI: MutableList<BusinessUI>): Aggregate<BusinessUI>, Iterator<BusinessUI> by businessesUI.iterator() {

    override fun count() = businessesUI.size

    override fun all() = businessesUI

    override fun get(position: Int) = businessesUI.get(position)

    override fun get(url: String): BusinessUI? {
        for (business in 0..businessesUI.count() -1) {
            if (businessesUI[business].url == url) {
                return businessesUI[business]
            }
        }
        return null
    }

    override fun add(element: BusinessUI) {
        businessesUI.add(element)
    }

    override fun delete(position: Int) {
        businessesUI.removeAt(position)
    }

    override fun delete(element: BusinessUI) {
        businessesUI.remove(element)
    }
}