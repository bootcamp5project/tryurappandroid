package com.tryurapp.tryurappandroid.tuamodel

data class ResponseBusinessUI(
        var status: String,
        var resultDescription: String,
        var result: BusinessUI
)