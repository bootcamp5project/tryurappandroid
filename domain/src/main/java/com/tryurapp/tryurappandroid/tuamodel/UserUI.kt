package com.tryurapp.tryurappandroid.tuamodel

data class UserUI(
        var email: String,
        var name: String,
        var password: String,
        var token: String
)