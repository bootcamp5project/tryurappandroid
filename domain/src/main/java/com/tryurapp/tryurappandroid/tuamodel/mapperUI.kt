package com.tryurapp.tryurappandroid.tuamodel

import com.tryurapp.tryurappandroid.repositorytua.model.BusinessTUA
import com.tryurapp.tryurappandroid.repositorytua.model.UserTUA

fun mapperUserUIToUserTUA(user: UserUI): UserTUA {
    return UserTUA(
            user.email,
            user.name,
            user.password
    )
}

fun mapperUserTUAToUserUI(user: UserTUA): UserUI {
    return UserUI(
            user.email,
            user.name,
            user.password,
            user.token
    )
}

fun mapperBusinessUIToBusinessTUA(business: BusinessUI): BusinessTUA {
    return BusinessTUA(
            business.userEmail,
            business.url,
            business.idBusinessType,
            business.idCmsType,
            business.idLoadingType,
            business.idAppIcon,
            business.firstColorIdentifier,
            business.secondColorIdentifier,
            business.thirdColorIdentifier,
            business.idFont,
            business.isValidInfo,
            business.logoDataName,
            business.welcomeImageDataName,
            business.buildApp,
            business.businessName

    )
}

fun mapperBusinessTUAToBusinessUI(business: BusinessTUA): BusinessUI {
    return BusinessUI(
            business.userEmail,
            business.url,
            business.idBusinessType,
            business.idCmsType,
            business.idLoadingType,
            business.idAppIcon,
            business.firstColorIdentifier,
            business.secondColorIdentifier,
            business.thirdColorIdentifier,
            business.idFont,
            business.isValidInfo,
            business.logoDataName,
            business.welcomeImageDataName,
            business.buildApp,
            business.businessName
    )
}

/*
fun mapperResponseApiUIToResponseApiTUA(response: ResponseApiTUA): ResponseApiUI {
    val userTUA = mapperUserTUAToUserUI(response.result!!)
    return ResponseApiUI(
            response.status,
            response.resultDescription!!,
            userTUA
    )
}

fun mapperResponseApiTUAToResponseApiUI(response: ResponseApiUI): ResponseApiTUA {
    val userUI = mapperUserUIToUserTUA(response.result)
    return ResponseApiTUA(
            response.status,
            response.resultDescription,
            userUI
    )
}

fun mapperResponseBusinessUIToResponseBusinessTUA(response: ResponseBusinessTUA): ResponseBusinessUI {
    val businessTUA = mapperBusinessTUAToBusinessUI(response.result!!)
    return ResponseBusinessUI(
            response.status.toString(),
            response.resultDescription!!,
            businessTUA
    )
}

fun mapperResponseBusinessTUAToResponseBusinessUI(response: ResponseBusinessUI): ResponseBusinessTUA {
    val businessUI = mapperBusinessUIToBusinessTUA(response.result)
    return ResponseBusinessTUA(
            response.status.toInt(),
            response.resultDescription,
            businessUI
    )
}
*/