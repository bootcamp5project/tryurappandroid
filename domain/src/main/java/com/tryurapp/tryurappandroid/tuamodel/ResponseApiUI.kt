package com.tryurapp.tryurappandroid.tuamodel

data class ResponseApiUI(
        var status: String,
        var resultDescription: String,
        var result: UserUI
)