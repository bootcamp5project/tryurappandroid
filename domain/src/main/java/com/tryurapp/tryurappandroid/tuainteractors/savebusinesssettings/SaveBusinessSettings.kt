package com.tryurapp.tryurappandroid.tuainteractors.savebusinesssettings

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessTUA

interface SaveBusinessSettingsInteractor {
    fun saveBusinessSettings(success: SuccessCompletion<ResponseBusinessTUA>,
                error: ErrorCompletion)
}