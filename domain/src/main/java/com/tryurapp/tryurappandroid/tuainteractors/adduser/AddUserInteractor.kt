package com.tryurapp.tryurappandroid.tuainteractors.adduser

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseApiTUA

interface AddUserInteractor {
    fun addUser(success: SuccessCompletion<ResponseApiTUA>,
                error: ErrorCompletion)
}