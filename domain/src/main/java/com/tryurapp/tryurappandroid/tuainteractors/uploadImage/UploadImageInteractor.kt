package com.tryurapp.tryurappandroid.tuainteractors.uploadImage

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessTUA

interface UploadImageInteractor {
    fun uploadImage(success: SuccessCompletion<ResponseBusinessTUA>,
                             error: ErrorCompletion)
}