package com.tryurapp.tryurappandroid.tuainteractors.getallusers

import android.content.Context
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.TUARepository
import com.tryurapp.tryurappandroid.repositorytua.TUARepositoryImpl
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseUsersApiTUA
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class GetAllUsersInteractorImpl(context: Context) : GetAllUsersInteractor {

    private val weakContext = WeakReference<Context>(context)
    private val repository: TUARepository = TUARepositoryImpl(weakContext.get()!!)

    override fun getAllUsers(success: SuccessCompletion<ResponseUsersApiTUA>, error: ErrorCompletion) {
        repository.getAllUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            success.successCompletion(it)
                        },
                        onError = {
                            error.errorCompletion(it.message!!)
                        }
                )
    }
}