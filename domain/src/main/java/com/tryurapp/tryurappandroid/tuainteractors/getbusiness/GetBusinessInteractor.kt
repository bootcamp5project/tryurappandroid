package com.tryurapp.tryurappandroid.tuainteractors.getbusiness

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessTUA

interface GetBusinessInteractor {
    fun getBusiness(success: SuccessCompletion<ResponseBusinessTUA>,
                    error: ErrorCompletion)
}