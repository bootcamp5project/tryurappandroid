package com.tryurapp.tryurappandroid.tuainteractors.getallbusiness

import android.content.Context
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.TUARepository
import com.tryurapp.tryurappandroid.repositorytua.TUARepositoryImpl
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessesTUA
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class GetAllBusinessInteractorImpl(context: Context) : GetAllBusinessInteractor {

    private val weakContext = WeakReference<Context>(context)
    private val repository: TUARepository = TUARepositoryImpl(weakContext.get()!!)

    override fun getAllBusiness(success: SuccessCompletion<ResponseBusinessesTUA>, error: ErrorCompletion) {

        repository.getAllBusiness()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            success.successCompletion(it)
                        },
                        onError = {
                            error.errorCompletion(it.message!!)
                        }
                )
    }
}