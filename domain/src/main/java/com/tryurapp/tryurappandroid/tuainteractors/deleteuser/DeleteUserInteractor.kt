package com.tryurapp.tryurappandroid.tuainteractors.deleteuser

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseApiTUA

interface DeleteUserInteractor {
    fun deleteUser(success: SuccessCompletion<ResponseApiTUA>,
                error: ErrorCompletion)
}