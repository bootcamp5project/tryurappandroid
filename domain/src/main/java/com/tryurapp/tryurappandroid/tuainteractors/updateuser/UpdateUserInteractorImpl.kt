package com.tryurapp.tryurappandroid.tuainteractors.updateuser

import android.content.Context
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.TUARepository
import com.tryurapp.tryurappandroid.repositorytua.TUARepositoryImpl
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseApiTUA
import com.tryurapp.tryurappandroid.tuainteractors.authenticateuser.AuthenticateUserInteractorImpl
import com.tryurapp.tryurappandroid.tuamodel.UserUI
import com.tryurapp.tryurappandroid.tuamodel.mapperUserUIToUserTUA
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class UpdateUserInteractorImpl(context: Context, user: UserUI) : UpdateUserInteractor {

    private val weakContext = WeakReference<Context>(context)
    private val repository: TUARepository = TUARepositoryImpl(weakContext.get()!!)
    private val userToUpdate = user

    override fun updateUser(success: SuccessCompletion<ResponseApiTUA>, error: ErrorCompletion) {
        //Recogemos el token del usuario a modificar
        val authUser = AuthenticateUserInteractorImpl(weakContext.get()!!, userToUpdate)
        authUser.authUser(
                success = object: SuccessCompletion<ResponseApiTUA> {
                    override fun successCompletion(e: ResponseApiTUA) {
                        var updateUser = mapperUserUIToUserTUA(userToUpdate)
                        updateUser.token = e.resultDescription!!
                        //updateUser.token = e.result!!
                        //Una vez que tenemos el usuario y su token, lanzamos la petición
                        repository.updateUser(updateUser)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeBy(
                                        onNext = {
                                            success.successCompletion(it)
                                        },
                                        onError = {
                                            error.errorCompletion(it.message!!)
                                        }
                                )
                    }
                }, error = object : ErrorCompletion {
                    override fun errorCompletion(errorMessage: String) {
                        error.errorCompletion(errorMessage)
                    }

                }
        )
    }
}