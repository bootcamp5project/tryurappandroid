package com.tryurapp.tryurappandroid.tuainteractors.getuser

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseApiTUA

interface GetUserInteractor {
    fun getUser(success: SuccessCompletion<ResponseApiTUA>,
                error: ErrorCompletion)
}