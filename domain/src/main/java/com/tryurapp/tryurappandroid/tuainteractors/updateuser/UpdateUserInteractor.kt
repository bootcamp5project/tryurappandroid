package com.tryurapp.tryurappandroid.tuainteractors.updateuser

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseApiTUA

interface UpdateUserInteractor {
    fun updateUser(success: SuccessCompletion<ResponseApiTUA>,
                error: ErrorCompletion)
}