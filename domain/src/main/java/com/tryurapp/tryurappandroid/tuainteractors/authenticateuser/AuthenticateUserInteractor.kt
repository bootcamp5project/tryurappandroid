package com.tryurapp.tryurappandroid.tuainteractors.authenticateuser

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseApiTUA

interface AuthenticateUserInteractor {
    fun authUser(success: SuccessCompletion<ResponseApiTUA>,
                 error: ErrorCompletion)
}