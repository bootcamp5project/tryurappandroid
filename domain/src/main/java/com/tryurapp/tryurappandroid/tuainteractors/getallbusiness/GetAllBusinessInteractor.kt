package com.tryurapp.tryurappandroid.tuainteractors.getallbusiness

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessesTUA

interface GetAllBusinessInteractor {
    fun getAllBusiness(success: SuccessCompletion<ResponseBusinessesTUA>,
                       error: ErrorCompletion)
}