package com.tryurapp.tryurappandroid.tuainteractors.buildapplication

import android.content.Context
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.TUARepository
import com.tryurapp.tryurappandroid.repositorytua.TUARepositoryImpl
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseApiTUA
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessTUA
import com.tryurapp.tryurappandroid.tuainteractors.authenticateuser.AuthenticateUserInteractorImpl
import com.tryurapp.tryurappandroid.tuamodel.BusinessUI
import com.tryurapp.tryurappandroid.tuamodel.UserUI
import com.tryurapp.tryurappandroid.tuamodel.mapperBusinessUIToBusinessTUA
import com.tryurapp.tryurappandroid.tuamodel.mapperUserUIToUserTUA
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class BuildApplicationInteractorImpl(context: Context,
                                     user: UserUI,
                                     business: BusinessUI)  : BuildApplicationInteractor {

    private val weakContext = WeakReference<Context>(context)
    private val repository: TUARepository = TUARepositoryImpl(weakContext.get()!!)
    private val userToAuth = user
    private val businessToAuth = business

    override fun buildApplication(success: SuccessCompletion<ResponseBusinessTUA>, error: ErrorCompletion) {

        //Recogemos el token del usuario a borrar
        val authUser = AuthenticateUserInteractorImpl(weakContext.get()!!, userToAuth)
        authUser.authUser(
                success = object: SuccessCompletion<ResponseApiTUA> {
                    override fun successCompletion(e: ResponseApiTUA) {
                        val userTUA = mapperUserUIToUserTUA(userToAuth)
                        userTUA.token = e.resultDescription!!
                        //Una vez que tenemos el usuario y su token, lanzamos la petición
                        repository.saveBusinessSettings(mapperBusinessUIToBusinessTUA(businessToAuth), userTUA)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeBy(
                                        onNext = {
                                            success.successCompletion(it)
                                        },
                                        onError = {
                                            error.errorCompletion(it.message!!)
                                        }
                                )
                    }
                }, error = object : ErrorCompletion {
            override fun errorCompletion(errorMessage: String) {
                error.errorCompletion(errorMessage)
            }

        }
        )
    }
}