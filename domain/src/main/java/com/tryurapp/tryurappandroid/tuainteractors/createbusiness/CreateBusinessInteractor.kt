package com.tryurapp.tryurappandroid.tuainteractors.createbusiness

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessTUA

interface CreateBusinessInteractor {
    fun createBusiness(success: SuccessCompletion<ResponseBusinessTUA>,
                error: ErrorCompletion)
}