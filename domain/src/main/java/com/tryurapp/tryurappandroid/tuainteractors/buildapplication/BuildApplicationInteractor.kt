package com.tryurapp.tryurappandroid.tuainteractors.buildapplication

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessTUA

interface BuildApplicationInteractor {
    fun buildApplication(success: SuccessCompletion<ResponseBusinessTUA>,
                             error: ErrorCompletion)
}