package com.tryurapp.tryurappandroid.tuainteractors.adduser

import android.content.Context
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.TUARepository
import com.tryurapp.tryurappandroid.repositorytua.TUARepositoryImpl
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseApiTUA
import com.tryurapp.tryurappandroid.tuamodel.UserUI
import com.tryurapp.tryurappandroid.tuamodel.mapperUserUIToUserTUA
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class AddUserInteractorImpl(context: Context, user: UserUI) : AddUserInteractor {

    private val weakContext = WeakReference<Context>(context)
    private val repository: TUARepository = TUARepositoryImpl(weakContext.get()!!)
    private val userToAuth = user

    override fun addUser(success: SuccessCompletion<ResponseApiTUA>,
                         error: ErrorCompletion) {

        repository.addUser(mapperUserUIToUserTUA(userToAuth))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            success.successCompletion(it)
                        },
                        onError = {
                            error.errorCompletion(it.message!!)
                        }
                )
    }
}