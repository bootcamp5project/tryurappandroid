package com.tryurapp.tryurappandroid.tuainteractors.getallusers

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseUsersApiTUA

interface GetAllUsersInteractor {
    fun getAllUsers(success: SuccessCompletion<ResponseUsersApiTUA>,
                    error: ErrorCompletion)
}