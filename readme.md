###ARQUITECTURA (VIPER)
* Módulo **app**

  Con la interface de usuario (UI)
  
  Solo ve **domain** y **util**
  
* Módulo **domain**

  Lógica de negocio. Casos de uso (interactors) y modelo de la UI.
  
  Solo ve **repository** y **util**
  
* Módulo **repository**

  Acceso a red, base de datos, cache y modelo de datos de la API.
  
  Solo ve **util**
  
* Módulo **util**

  Clases y métodos generales.
  
  No ve a nadie
  

###DEPENDENCIAS
* **RxJava** como lenguaje de programación
* **Retrofit** como libreria de comunicaciones
* **Gson** como parseador de Json
* **Picasso** como libreria de descarga de imágenes
* **Realm** como base de datos
  

###WEBSERVICE ANDROID

En **WordpressService** se definen los verbos de la petición y los endpoints
para la comunicación con Wordpress y Woocommerce.

En **WordpressApiService** se realiza la llamada al servicio. Aquí, se añade 
la *URL_BASE*, dentro de longging los testeos que se desean de los *Headers*
y en *client* se añaden las key_api, tokens y elementos de identificación con
la API.

En **TUAService** se definen los verbos de la petición y los endpoints para la
comunicación con TryUrApp.

En **TUAApiService** se realiza la llamada al servicio. Aquí, se añade 
la *URL_BASE*, dentro de longging los testeos que se desean de los *Headers*
y en *client* se añade el token como elemento de identificación con
la API.

###USO

####Para Wordpress y Woocommerce

Se usa, en la implementación del *Repository*, como:

     TUAApiService().tuaService().getUsers()

####Para TryUrApp

Se usa, en la implementación del *TUARepository*, como:

     WordpressApiService().wpService().getPosts()

Para añadir un caso nuevo se implementaría el método en **TUAService**
y se le llamaría de la forma especificada arriba. Al definir el método, definiremos
el tipo de *Observable* de salida (devolverá un observable del tipo deseado).

Para subscribirnos al observable, haremos:

     tuaRepository.getUsers()
          .subscribeOn(schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribeBy(
               onNext = { observable ->

               },
               onError = { error ->

               }
          ) 

Esto lo implementaremos desde los **interactors** (casos de uso) del **domain**

###INTERACTORS

Para la abstracción desde la **UI** con los datos y su almacenamiento, hay dos grupos de 
**interactors**, uno para cada API.

####Interactors para Wordpress

Los interactors para Wordpress son:

* **GetPost**

    Trae un **post** determinado del blog
        
        GetPostInteractorImpl(context: Context, postId: Int)
                    .loadPost(success: SuccessCompletion<PostWP>,
                              error: ErrorCompletion)

    Devuelve el post solicitado si existe o el error producido en caso de error.

* **GetAllPosts**

    Traer todos los **posts** editados del blog.
    
        GetAllPostsInteractorImpl(context: Context)
                    .loadPosts(success: SuccessCompletion<PostsUI>,
                               error: ErrorCompletion)

    Devuelve una lista de Posts si todo ha ido bien o un error si ha habido algún
    problema, junto a la definición del error
    
    
* **GetProduct**

    Trae un **product** de la tienda.
            
        GetProductInteractorImpl(context: Context, productId: Int)
                     .loadProduct(success: SuccessCompletion<ProductUI>,
                                  error: ErrorCompletion)
    
    Devuelve el product solicitado si existe o el error producido en caso de error

* **GetAllProducts**

    Trae todos los **products** de la tienda.
                
        GetProductsInteractorImpl(context: Context)
                     .loadProduct(success: SuccessCompletion<ProductUI>,
                                  error: ErrorCompletion)
            
    Devuelve una lista de Products si todo ha ido bien o un error si ha habido algún
    problema, junto a la definición del error


####Interactors para TryUrApp

Los interactors para TryUrApp son:

* **GetUser**
    
    Trae un usuario de la base de datos de usuario. Se le pasa un usuario con email 
    informado.
    
        GetUserInteractorImpl(context: Context, user: UserUI)
                 .getUser(success: SuccessCompletion<ResponseApiTUAObject>, 
                          error: ErrorCompletion)

    Devuelve si existe el usuario deseado o el error producido en su caso
     
     
* **GetAllUsers**

    Trae todos los usuarios de la base de datos.
   
        GetAllUsersInteractorImpl(context: Context)
                 .getUser(success: SuccessCompletion<ResponseApiTUAObject>, 
                          error: ErrorCompletion)
                                             
    Devuelve una lista con los usuarios existentes en la base de datos, o el error
    producido en su caso.
 
    
* **AddUser**

    Almacena un usuario en la base de datos.
    
        AddUserInteractorImpl(context: Context, user: UserUI)
                  .addUser(success: SuccessCompletion<ResponseApiTUAObject>,
                           error: ErrorCompletion)
                           
    Devuelve el usuario almacenado si ha ido bien o el error producido en su caso.
    

* **AuthenticateUser**

    Autentica un usuario. Se le pasa un usuario con el email y el password informados.
    
        AuthenticateUserInteractorImpl(context: Context, user: UserUI)
                  .authUser(success: SuccessCompletion<ResponseApiTUA>,
                            error: ErrorCompletion)
                            
    Devuelve un token para el usuario o el error producido si es el caso.


* **UpdateUser**

    Modifica los datos de un usuario. Se le pasa un usuario con el email y el password 
    informados y los datos que se quieren modificar
    
        UpdateUserInteractorImpl(context: Context, user: UserUI)
                   .updateUser(success: SuccessCompletion<ResponseApiTUA>, 
                               error: ErrorCompletion)
        
    Devuelve un mensaje con el resultado de la operación


* **DeleteUser**

    Borra los datos de un usuario. Se le pasa el usuario a borrar con el email y el password 
    informados.
    
        DeleteUserInteractorImpl(context: Context, user: UserUI)
                    .deleteUser(success: SuccessCompletion<ResponseApiTUA>, 
                                error: ErrorCompletion)
        
    Devuelve un mensaje con el resultado de la operación.


###AUTENTICACION

Autenticación con WooCommerce implementada. Basic Auth con **consumer_key** y **consumer_secret**.

Autenticación con TryUrApp implementada. Con **token**

###REPOSITORY

Definidos dos repositorios separados. Uno para Wordpress (**Repository**) y otro para
TryUrApp (**RepositoryTUA**).

Los métodos de los repositorios son.

####Para el repositorio de Wordpress

Traer los productos de la tienda

    fun getProducts(): Flowable<List<ProductWP>>

Traer un producto de la tienda

    fun getProduct(productId: Int): Flowable<ProductWP>

Traer todos los posts del blog

    fun getPosts(): Flowable<List<PostWP>>

Traer un post del blog

    fun getPost(postId: Int): Flowable<PostWP>

Añadir un post al blog

    fun addPost(post: PostBody): Flowable<ResponseApi>


####Para el repositorio de TryUrApp

Autorizar un usuario (pedir su token)

    fun authUser(userTUA: UserTUA): Flowable<ResponseApiTUA>

Traer todos los usuarios de la base de datos

    fun getAllUsers(): Flowable<ResponseApiTUA>

Añadir un usuario a la base de datos

    fun addUser(user: UserTUA): Flowable<ResponseApiTUAObject>

Traer un usuario de la base de datos

    fun getUser(user: UserTUA): Flowable<ResponseApiTUAObject>

Modificar los datos de un usuario en la base de datos

    fun updateUser(user: UserTUA): Flowable<ResponseApiTUA>

Borrar un usuario de la base de datos

    fun deleteUser(user: UserTUA): Flowable<ResponseApiTUA>