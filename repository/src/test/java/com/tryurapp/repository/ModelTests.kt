package com.tryurapp.repository

import com.tryurapp.tryurappandroid.repository.model.*
import org.junit.Assert
import org.junit.Test
import org.junit.experimental.categories.Category

class ModelTests {

    var product1 = ProductWP(1, "Producto1")
    val categorie = Categories(1, "Ropa de cama")
    val categories: List<Categories> = listOf(categorie)

    val title = Rendered("Post 1")
    var post1 = PostWP(1, title)

    fun initializeInformation() {
        product1.categories = categories
        val newTitle = Rendered("New post 1")
        post1.title = newTitle
    }

    @Test
    @Throws(Exception::class)
    fun given_a_productWP_When_we_changes_the_category_Then_the_categorie_changes() {
        initializeInformation()
        Assert.assertEquals("Ropa de cama", product1.categories!![0].name)
    }

    @Test
    @Throws(Exception::class)
    fun given_a_postWP_When_we_changes_the_title_Then_the_title_changes() {
        initializeInformation()
        Assert.assertEquals("New post 1", post1.title!!.rendered)
    }
}