package com.tryurapp.tryurappandroid.repository

import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repository.db.model.ProductObject
import com.tryurapp.tryurappandroid.repository.model.PostBody

import com.tryurapp.tryurappandroid.repository.model.PostWP
import com.tryurapp.tryurappandroid.repository.model.ProductWP
import com.tryurapp.tryurappandroid.repository.model.ResponseApi
import io.reactivex.Flowable

interface WPRepository {
    fun getProducts(success: SuccessCompletion<Flowable<List<ProductObject>>>, error: ErrorCompletion)
    fun getProduct(productId: Int): Flowable<ProductWP>
    fun getPosts(): Flowable<List<PostWP>>
    fun getPost(postId: Int): Flowable<PostWP>
    fun addPost(post: PostBody): Flowable<ResponseApi>
}

