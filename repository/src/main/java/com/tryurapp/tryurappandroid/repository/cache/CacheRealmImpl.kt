package com.tryurapp.tryurappandroid.repository.cache

import android.content.Context
import com.tryurapp.tryurappandroid.repository.db.model.ProductObject
import com.vicpin.krealmextensions.queryAllAsFlowable
import com.vicpin.krealmextensions.saveAll
import io.reactivex.Flowable
import io.realm.Realm
import java.lang.ref.WeakReference

internal class CacheRealmImpl(context: Context): Cache {
    private val weakContext = WeakReference<Context>(context)
    override fun writeAllProducts(list: ArrayList<ProductObject>) {
        Realm.init(weakContext.get()!!)
        list.saveAll()
    }

    override fun getAllProducts(): Flowable<List<ProductObject>> {
        Realm.init(weakContext.get()!!)
        return ProductObject().queryAllAsFlowable()
    }

}