package com.tryurapp.tryurappandroid.repository.db.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ProductObject: RealmObject() {
    @PrimaryKey
    public var id: Int = 0
    public var title: String = ""
    public var image: String = ""
    public var salePrice: String = ""

}