package com.tryurapp.tryurappandroid.repository

import android.content.Context
import com.tryurapp.tryurappandroid.ErrorCompletion
import com.tryurapp.tryurappandroid.SuccessCompletion
import com.tryurapp.tryurappandroid.repository.cache.Cache
import com.tryurapp.tryurappandroid.repository.cache.CacheRealmImpl
import com.tryurapp.tryurappandroid.repository.db.model.ProductObject
import com.tryurapp.tryurappandroid.repository.model.*
import com.tryurapp.tryurappandroid.repository.network.webservice.WordpressApiService
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.toFlowable
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class WPRepositoryImpl(context: Context): WPRepository {

    private val weakContext = WeakReference<Context>(context)
    private val cache: Cache = CacheRealmImpl(context)

    override fun getProducts(success: SuccessCompletion<Flowable<List<ProductObject>>>, error: ErrorCompletion) {
        cache.getAllProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            println(it)
                            if (it.count() > 0) {
                                success.successCompletion(Flowable.fromArray(it))
                            } else {
                                populateProducts(success, error)
                            }

                        },
                        onError = {
                            populateProducts(success, error)
                        }
                )
    }

    private fun populateProducts(success: SuccessCompletion<Flowable<List<ProductObject>>>, error: ErrorCompletion) {
        val resultsObs = WordpressApiService().wpService().getProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            println(it)
                            val listProductObject = ArrayList<ProductObject>()
                            it.map {
                                listProductObject.add(it.managedObject())
                            }
                            cache.writeAllProducts(listProductObject)
                            success.successCompletion(Flowable.fromArray(listProductObject))
                        },
                        onError = {
                            error.errorCompletion(it.message!!)
                        }
                )
    }


    override fun getProduct(productId: Int): Flowable<ProductWP> =
            WordpressApiService().wpService().getProduct(productId)

    override fun getPosts(): Flowable<List<PostWP>> =
            WordpressApiService().wpService().getPosts()

    override fun getPost(postId: Int): Flowable<PostWP> =
            WordpressApiService().wpService().getPost(postId)

    override fun addPost(post: PostBody): Flowable<ResponseApi> =
            WordpressApiService().wpService().addPost(post)

}
