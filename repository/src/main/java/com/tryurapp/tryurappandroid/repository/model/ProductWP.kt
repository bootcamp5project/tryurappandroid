package com.tryurapp.tryurappandroid.repository.model

import com.google.gson.annotations.SerializedName
import com.tryurapp.tryurappandroid.repository.db.model.ProductObject

data class ProductWP(
        @SerializedName("id")
        var identifier: Int,
        var name: String,
        var slug: String = "",
        var permalink: String = "",
        @SerializedName("date_created")
        var dateCreated: String = "",
        @SerializedName("date_created_gtm")
        var dateCreatedGtm: String = "",
        @SerializedName("date_modified")
        var dateModified: String = "",
        @SerializedName("date_modified_gtm")
        var dateModifiedGtm: String = "",
        var type: String = "",
        var status: String = "",
        var featured: Boolean = true,
        @SerializedName("catalog_visibility")
        var catalogVisibility: String = "",
        var description: String = "",
        @SerializedName("short_description")
        var shortDescription: String = "",
        var sku: String = "",
        var price: String = "",
        @SerializedName("regular_price")
        var regularPrice: String = "",
        @SerializedName("sale_price")
        var salePrice: String = "",
        @SerializedName("date_on_sale_from")
        var dateOnSaleFrom: String = "",
        @SerializedName("date_on_sale_from_gtm")
        var dateOnSaleFromGtm: String = "",
        @SerializedName("date_on_sale_to")
        var dateOnSaleTo: String = "",
        @SerializedName("date_on_sale_to_gtm")
        var dateOnSaleToGtm: String = "",
        @SerializedName("price_html")
        var priceHtml: String = "",
        @SerializedName("on_sale")
        var onSale: Boolean = true,
        var purchasable: Boolean = true,
        @SerializedName("total_sales")
        var totalSales: Double = 0.0,
        var virtual: Boolean = true,
        var downloadable: Boolean = true,
        var downloads: List<Int>? = null,
        @SerializedName("download_limit")
        var downloadLimit: Int = 0,
        @SerializedName("download_expiry")
        var downloadExpiry: Int = 0,
        @SerializedName("external_url")
        var externalUrl: String = "",
        @SerializedName("button_text")
        var buttonText: String = "",
        @SerializedName("tax_status")
        var taxStatus: String = "",
        @SerializedName("tax_class")
        var taxClass: String = "",
        @SerializedName("manage_stock")
        var manageStock: Boolean = true,
        @SerializedName("stock_quantity")
        var stockQuantity: String = "",
        @SerializedName("in_stock")
        var inStock: Boolean = true,
        var backorders: String = "",
        @SerializedName("backorders_allowed")
        var backordersAllowed: Boolean = true,
        var backordered: Boolean = true,
        @SerializedName("sold_individually")
        var soldIndividually: Boolean = true,
        var weight: String = "",
        var dimensions: Dimensions? = null,
        @SerializedName("shipping_required")
        var shippingRequired: Boolean = true,
        @SerializedName("shipping_taxable")
        var shippingTaxable: Boolean = true,
        @SerializedName("shipping_class")
        var shippingClass: String = "",
        @SerializedName("shipping_calss_id")
        var shippingClassId: Int = 0,
        @SerializedName("reviews_allowed")
        var reviewsAllowed: Boolean = true,
        @SerializedName("average_rating")
        var averageRating: Double = 0.0,
        @SerializedName("rating_count")
        var ratingCount: Int = 0,
        @SerializedName("related_ids")
        var relatedIds: List<Int>? = null,
        @SerializedName("upsell_ids")
        var upsellIds: List<Int>? = null,
        @SerializedName("cross_sell_ids")
        var crossSellIds: List<Int>? = null,
        @SerializedName("parent_id")
        var parentId: Int = 0,
        @SerializedName("purchase_note")
        var purchaseNote: String = "",
        var categories: List<Categories>? = null,
        var tags: List<String>? = null,
        var images: List<Images>? = null,
        var attributes: List<String>? = null,
        @SerializedName("default_attributes")
        var defaultAttributes: List<String>? = null,
        var variations: List<String>? = null,
        @SerializedName("grouped_produts")
        var groupedProduts: List<String>? = null,
        @SerializedName("menu_order")
        var menuOrder: Int = 0,
        @SerializedName("meta_data")
        var metaData: List<MetaData>? = null,
        @SerializedName("_links")
        var links: Links? = null
)

data class Dimensions(
        var length: String = "",
        var width: String = "",
        var height: String = ""
)

data class Categories(
        var id: Int = 0,
        var name: String = "",
        var slug: String = ""
)

data class Images(
        var id: Int,
        @SerializedName("date_created")
        var dateCreated: String = "",
        @SerializedName("date_created_gtm")
        var dateCreatedGtm: String = "",
        @SerializedName("date_modified")
        var dateModified: String = "",
        @SerializedName("date_modified_gtm")
        var dateModifiedGtm: String = "",
        var src: String = "",
        var name: String = "",
        var alt: String = "",
        var position: Int = 0
)

data class MetaData(
        var id: Int = 0,
        var key: String = "",
        var value: String = ""
)

data class Links(
        var self: List<Href1>? = null,
        var collection: List<Href1>? = null
)

data class Href1(
        var href: String = ""
)

public fun ProductWP.managedObject(): ProductObject {
        var product = ProductObject()

        product.id = identifier
        product.title = name
        product.image = images!!.get(0).src
        product.salePrice = salePrice

        return product
}