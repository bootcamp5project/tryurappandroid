package com.tryurapp.tryurappandroid.repository.network.webservice

import com.tryurapp.tryurappandroid.CONSUMER_KEY
import com.tryurapp.tryurappandroid.CONSUMER_SECRET
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class WordpressApiService {

    private fun provideRetrofit(): Retrofit {

        val longging = Interceptor { chain ->
            val request = chain.request()
            val originalHttpURL = request.url()

            val finalURL = originalHttpURL.newBuilder()
                    .addQueryParameter("consumer_key", CONSUMER_KEY)
                    .addQueryParameter("consumer_secret", CONSUMER_SECRET)
                    .build()

            val finalRequest = request.newBuilder()
                    .url(finalURL)
                    .build()

            chain.proceed(finalRequest)
        }

        val client = OkHttpClient.Builder()
                .connectTimeout(5L, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                .addInterceptor(longging)
                .build()

        return Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("https://blog.tryurapp.com/wp-json/")
                .build()
    }

    fun wpService() = provideRetrofit().create(WordpressService::class.java)

}