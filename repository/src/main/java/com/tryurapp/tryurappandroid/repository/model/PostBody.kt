package com.tryurapp.tryurappandroid.repository.model

data class PostBody(
        var title: String
)