package com.tryurapp.tryurappandroid.repository.model

data class ResponseApi(
       var code: String,
       var message: String,
       var data: Status
)

data class Status(
        var status: Int
)