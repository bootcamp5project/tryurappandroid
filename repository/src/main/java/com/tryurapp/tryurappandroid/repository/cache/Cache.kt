package com.tryurapp.tryurappandroid.repository.cache

import com.tryurapp.tryurappandroid.repository.db.model.ProductObject
import io.reactivex.Flowable
import io.reactivex.Observable

internal interface Cache {
    fun getAllProducts(): Flowable<List<ProductObject>>
    fun writeAllProducts(list: ArrayList<ProductObject>)
}