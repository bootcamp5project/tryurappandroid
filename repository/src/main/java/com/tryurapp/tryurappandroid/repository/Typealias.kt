package com.tryurapp.tryurappandroid.repository

import com.tryurapp.tryurappandroid.repository.model.ProductWP

typealias SuccessClosure = (productsWP: List<ProductWP>) -> Unit
typealias ErrorClosure = (msg: String) -> Unit