package com.tryurapp.tryurappandroid.repository.model

import com.google.gson.annotations.SerializedName

data class PostWP(
    @SerializedName("id")
    var identifier: Int,
    var title: Rendered? = null,
    var date: String = "",
    @SerializedName("date_gtm")
    var dateGtm: String = "",
    var guid: Rendered? = null,
    var modified: String = "",
    @SerializedName("modified_gtm")
    var modifiedGtm: String = "",
    var slug: String = "",
    var status: String = "",
    var type: String = "",
    var link: String = "",
    var content: Rendered? = null,
    var except: Rendered? = null,
    @SerializedName("featured_media")
    var featuredMedia: Int = 0,
    @SerializedName("comment_status")
    var commentStatus: String = "",
    @SerializedName("ping_status")
    var pingStatus: String = "",
    var template: String = "",
    var meta: List<String>? = null,
    var _links: Link? = null
)

data class Rendered(
        var rendered: String = "",
        var protected: Boolean = true
)

data class Link(
    @SerializedName("self_link")
    var selfLink: List<Href>? = null,
    var collection: List<Href>? = null,
    var about: List<Href>? = null,
    var replies: List<Href>? = null,
    //var featured_media: List<Href>
    //var attachment: List<Href>
    var curies: List<Href>? = null
)

data class Href(
        var href: String = "",
        var embedable: Boolean = true,
        var templated: Boolean = true,
        var name: String = ""
)

