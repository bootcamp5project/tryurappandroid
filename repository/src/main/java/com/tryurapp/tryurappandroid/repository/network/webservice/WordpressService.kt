package com.tryurapp.tryurappandroid.repository.network.webservice

import com.tryurapp.tryurappandroid.repository.model.PostBody
import com.tryurapp.tryurappandroid.repository.model.PostWP
import com.tryurapp.tryurappandroid.repository.model.ProductWP
import com.tryurapp.tryurappandroid.repository.model.ResponseApi
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.GET

import retrofit2.http.Path

import retrofit2.http.POST


interface WordpressService {

    @GET("wc/v2/products")
    fun getProducts(): Flowable<List<ProductWP>>

    @GET("wp/v2/product/{productId}")
    fun getProduct(@Path("productId") productId: Int): Flowable<ProductWP>

    @GET("wp/v2/posts")
    fun getPosts(): Flowable<List<PostWP>>

    @GET("wp/v2/posts/{postId}")
    fun getPost(@Path("postId") post: Int): Flowable<PostWP>

    @POST("wp/v2/posts")
    fun addPost(@Body postBody: PostBody): Flowable<ResponseApi>

}