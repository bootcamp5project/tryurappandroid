package com.tryurapp.tryurappandroid.repositorytua.model

data class BusinessTUA(
        var userEmail: String = "",
        var url: String = "",
        var idBusinessType: Int = 0,
        var idCmsType: Int = 0,
        var idLoadingType: Int = 0,
        var idAppIcon: Int = 0,
        var firstColorIdentifier: Int = 0,
        var secondColorIdentifier: Int = 0,
        var thirdColorIdentifier: Int = 0,
        var idFont: Int = 0,
        var isValidInfo: Boolean,
        var logoDataName: String,
        var welcomeImageDataName: String,
        var buildApp: String,
        var businessName: String
)