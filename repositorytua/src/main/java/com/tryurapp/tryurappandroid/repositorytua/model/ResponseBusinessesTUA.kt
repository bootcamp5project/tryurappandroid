package com.tryurapp.tryurappandroid.repositorytua.model

class ResponseBusinessesTUA(
        var status: Int = 0,
        var resultDescription: String = "",
        var result: List<BusinessTUA>? = null
)