package com.tryurapp.tryurappandroid.repositorytua.model

data class UserTUA(
        var email: String,
        var name: String,
        var password: String
) {
    var token: String = ""
    var _id: String = ""
    var creationDate: String? = ""
    var deletedDate: String? = ""
    var lastModifiedDate: String? = ""
    var isActive: Boolean? = true
}