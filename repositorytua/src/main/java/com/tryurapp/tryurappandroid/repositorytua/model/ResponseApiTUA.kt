package com.tryurapp.tryurappandroid.repositorytua.model

class ResponseApiTUA(
        var status: String,
        var resultDescription: String? = "",
        var result: UserTUA? = null
)
