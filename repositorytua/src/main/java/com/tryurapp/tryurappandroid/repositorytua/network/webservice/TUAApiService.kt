package com.tryurapp.tryurappandroid.repositorytua.network.webservice

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class TUAApiService {

    private fun provideRetrofit(token: String): Retrofit {

        val longging = Interceptor { chain ->
            val request = chain.request()
            val originalHttpURL = request.url()

            val finalURL = originalHttpURL.newBuilder()
                    .addQueryParameter("token", token)
                    .build()

            val finalRequest = request.newBuilder()
                    .url(finalURL)
                    .build()

            chain.proceed(finalRequest)
        }

        val client = OkHttpClient.Builder()
                .connectTimeout(5L, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                .addInterceptor(longging)
                .build()

        return Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                .baseUrl("https://api.tryurapp.com/api/")
                .build()
    }

    fun tuaService(token: String) = provideRetrofit(token).create(TUAService::class.java)

}