package com.tryurapp.tryurappandroid.repositorytua.model

class ResponseUsersApiTUA(
        var status: String,
        var resultDescription: String? = "",
        var result: List<UserTUA>? = null
)