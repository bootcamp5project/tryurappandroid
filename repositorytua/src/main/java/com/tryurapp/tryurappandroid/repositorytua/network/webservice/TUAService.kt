package com.tryurapp.tryurappandroid.repositorytua.network.webservice

import com.tryurapp.tryurappandroid.repositorytua.model.ResponseApiTUA
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessTUA
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseBusinessesTUA
import com.tryurapp.tryurappandroid.repositorytua.model.ResponseUsersApiTUA
import io.reactivex.Flowable
import okhttp3.RequestBody
import retrofit2.http.*

interface TUAService {

    //USERS
    @GET("users/authenticate")
    fun authUser(@Query("email") email: String, @Query("password") password: String): Flowable<ResponseApiTUA>

    @POST("users/update")
    @FormUrlEncoded
    fun updateUser(@Field("email") email: String,
                   @Field("name") name: String,
                   @Field("password") password: String): Flowable<ResponseApiTUA>

    @POST("users/delete")
    @FormUrlEncoded
    fun deleteUser(@Field("email") email: String): Flowable<ResponseApiTUA>

    @GET("users/all/")
    fun getAllUsers(): Flowable<ResponseUsersApiTUA>

    @POST("users/register")
    @FormUrlEncoded
    fun addUser(@Field("email") email: String,
                @Field("name") name: String,
                @Field("password") password: String): Flowable<ResponseApiTUA>

    @GET("users/")
    fun getUser(@Query("email") email: String): Flowable<ResponseApiTUA>

    //BUSINESS
    @GET("businesses/")
    fun getBusiness(@Query("url") url: String): Flowable<ResponseBusinessTUA>

    @GET("businesses/all/")
    fun getAllBusiness(): Flowable<ResponseBusinessesTUA>

    @POST("businesses/create")
    @FormUrlEncoded
    fun createBusiness(@Field("userEmail") userEmail: String,
                       @Field("url") url: String,
                       @Field("idBusinessType") idBusinessType: Int,
                       @Field("idCmsType") idCmsType: Int): Flowable<ResponseBusinessTUA>

    @POST("businesses/savesettings")
    @FormUrlEncoded
    fun saveBusinessSettings(@Field("userEmail") userEmail: String,
                             @Field("url") url: String,
                             @Field("idLoadingType") idLoadingType: Int,
                             @Field("idAppIcon") idAppIcon: Int,
                             @Field("firstColorIdentifier") firstColorIdentifier: Int,
                             @Field("secondColorIdentifier") secondColorIdentifier: Int,
                             @Field("thirdColorIdentifier") thirdColorIdentifier: Int,
                             @Field("idFont") idFont: Int,
                             @Field("buildApp") buildApp: String,
                             @Field("businessName") businessName: String): Flowable<ResponseBusinessTUA>

    @Multipart
    @POST("businesses/uploadImage")
    fun uploadImage(@Part("imageType") imageType: RequestBody,
                    @Part("url") url: RequestBody,
                    @Part("image") image: RequestBody): Flowable<ResponseBusinessTUA>

    @POST("businesses/savesettings")
    @FormUrlEncoded
    fun buildApplication(@Field("userEmail") userEmail: String,
                             @Field("url") url: String,
                             @Field("idLoadingType") idLoadingType: Int,
                             @Field("idAppIcon") idAppIcon: Int,
                             @Field("firstColorIdentifier") firstColorIdentifier: Int,
                             @Field("secondColorIdentifier") secondColorIdentifier: Int,
                             @Field("thirdColorIdentifier") thirdColorIdentifier: Int,
                             @Field("idFont") idFont: Int,
                             @Field("buildApp") buildApp: String,
                             @Field("businessName") businessName: String): Flowable<ResponseBusinessTUA>


}