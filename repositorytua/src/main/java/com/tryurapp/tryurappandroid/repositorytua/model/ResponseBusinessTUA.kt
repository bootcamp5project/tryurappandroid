package com.tryurapp.tryurappandroid.repositorytua.model

class ResponseBusinessTUA(
    var status: Int = 0,
    var resultDescription: String = "",
    var result: BusinessTUA? = null
)