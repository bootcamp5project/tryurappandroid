package com.tryurapp.tryurappandroid.repositorytua

import com.tryurapp.tryurappandroid.repositorytua.model.*
import io.reactivex.Flowable

interface TUARepository {
    //USERS
    fun authUser(userTUA: UserTUA): Flowable<ResponseApiTUA>
    fun getAllUsers(): Flowable<ResponseUsersApiTUA>
    fun addUser(user: UserTUA): Flowable<ResponseApiTUA>
    fun getUser(user: UserTUA): Flowable<ResponseApiTUA>
    fun updateUser(user: UserTUA): Flowable<ResponseApiTUA>
    fun deleteUser(user: UserTUA): Flowable<ResponseApiTUA>

    //BUSINESS
    fun createBusiness(businessTUA: BusinessTUA, user: UserTUA): Flowable<ResponseBusinessTUA>
    fun saveBusinessSettings(businessTUA: BusinessTUA, user: UserTUA): Flowable<ResponseBusinessTUA>
    fun getBusiness(businessTUA: BusinessTUA, user: UserTUA): Flowable<ResponseBusinessTUA>
    fun getAllBusiness(): Flowable<ResponseBusinessesTUA>
    fun uploadImage(image: String, imageType: String, business: BusinessTUA, user: UserTUA): Flowable<ResponseBusinessTUA>
    fun buildApplication(businessTUA: BusinessTUA, user: UserTUA): Flowable<ResponseBusinessTUA>
}