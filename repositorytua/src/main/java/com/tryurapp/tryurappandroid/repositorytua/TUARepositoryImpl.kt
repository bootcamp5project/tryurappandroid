package com.tryurapp.tryurappandroid.repositorytua

import android.content.Context
import com.tryurapp.tryurappandroid.repositorytua.model.*
import com.tryurapp.tryurappandroid.repositorytua.network.webservice.TUAApiService
import io.reactivex.Flowable
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import java.lang.ref.WeakReference

class TUARepositoryImpl(context: Context): TUARepository {

    private val weakContext = WeakReference<Context>(context)

    //USERS
    override fun authUser(user: UserTUA): Flowable<ResponseApiTUA> =
            TUAApiService().tuaService(user.token).authUser(user.email, user.password)

    override fun getAllUsers(): Flowable<ResponseUsersApiTUA> =
            TUAApiService().tuaService("").getAllUsers()

    override fun addUser(user: UserTUA): Flowable<ResponseApiTUA> =
            TUAApiService().tuaService("").addUser(user.email, user.name, user.password)

    override fun getUser(user: UserTUA): Flowable<ResponseApiTUA> =
            TUAApiService().tuaService(user.token).getUser(user.email)

    override fun updateUser(user: UserTUA): Flowable<ResponseApiTUA> =
            TUAApiService().tuaService(user.token).updateUser(user.email, user.name, user.password)

    override fun deleteUser(user: UserTUA): Flowable<ResponseApiTUA> =
            TUAApiService().tuaService(user.token).deleteUser(user.email)

    //BUSINESS
    override fun createBusiness(businessTUA: BusinessTUA, user: UserTUA): Flowable<ResponseBusinessTUA> =
            TUAApiService().tuaService(user.token).createBusiness(businessTUA.userEmail, businessTUA.url,
                    businessTUA.idBusinessType, businessTUA.idCmsType)

    override fun saveBusinessSettings(businessTUA: BusinessTUA, user: UserTUA): Flowable<ResponseBusinessTUA> =
            TUAApiService().tuaService(user.token).saveBusinessSettings(businessTUA.userEmail, businessTUA.url,
                    businessTUA.idLoadingType, businessTUA.idAppIcon, businessTUA.firstColorIdentifier,
                    businessTUA.secondColorIdentifier, businessTUA.thirdColorIdentifier, businessTUA.idFont,
                    "false", businessTUA.businessName)

    override fun getBusiness(businessTUA: BusinessTUA, user: UserTUA): Flowable<ResponseBusinessTUA> =
            TUAApiService().tuaService(user.token).getBusiness(businessTUA.url)

    override fun getAllBusiness(): Flowable<ResponseBusinessesTUA> =
            TUAApiService().tuaService("").getAllBusiness()

    override fun uploadImage(image: String, imageType: String, business: BusinessTUA, user: UserTUA): Flowable<ResponseBusinessTUA> {
        val pfile = File(".src/res/raw" + image)
        var imageRB = RequestBody.create(MediaType.parse("image/png"), pfile)
        var urlRB = RequestBody.create(MediaType.parse("text/plain"), business.url)
        var imageTypeRB = RequestBody.create(MediaType.parse("text/plain"), imageType)

        return TUAApiService().tuaService(user.token).uploadImage(imageTypeRB, urlRB, imageRB)
    }

    override fun buildApplication(businessTUA: BusinessTUA, user: UserTUA): Flowable<ResponseBusinessTUA> =
            TUAApiService().tuaService(user.token).buildApplication(businessTUA.userEmail, businessTUA.url,
                    businessTUA.idLoadingType, businessTUA.idAppIcon, businessTUA.firstColorIdentifier,
                    businessTUA.secondColorIdentifier, businessTUA.thirdColorIdentifier, businessTUA.idFont,
                    businessTUA.buildApp, businessTUA.businessName)



}